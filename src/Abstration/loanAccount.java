package Abstration;

public class loanAccount implements Account{
    double loanAmount=0.0;
    public loanAccount(double loanAmount)
    {
        this.loanAmount=loanAmount;
        System.out.println("loan account created");
    }
    @Override
    public void deposit(double amt) {
        loanAmount-=amt;
        System.out.println(amt+"rs debited from loan account");

    }

    @Override
    public void withdraw(double amt) {
        loanAmount+=amt;
        System.out.println(amt+"rs creadited to your account");

    }

    @Override
    public void checkBalance() {
        System.out.println("active loan account"+loanAmount+"rs");

    }
}
