package Abstration;



import java.util.Scanner;

public class MainApp1 {
    public static void main(String[] args) {
        Scanner sc1=new Scanner(System.in);
        System.out.println("select account type");
        System.out.println("1.saving \t\t 2.loan");
        int choice = sc1.nextInt();
        System.out.println("enter account opening balance");
        double balance=sc1.nextDouble();

        AccountFactory factory=new AccountFactory();
        Account accRef= factory.createAccount(choice,balance);

        System.out.println("==================================================");
        boolean status=true;
        while(status)
        {
            System.out.println("select mode of transaction");
            System.out.println("1.deposit");
            System.out.println("2.withdraw");
            System.out.println("3.check balance");
            System.out.println("4.exit");
            int mode= sc1.nextInt();

            switch(mode)
            {
                case 1:
                    System.out.println("enter amount");
                    double depositAmt=sc1.nextDouble();
                    accRef.deposit(depositAmt);
                    break;

                case 2:
                    System.out.println("enter amount");
                    double withdrawAmt=sc1.nextDouble();
                    accRef.checkBalance();
                    break;

                case 3:
                    accRef.checkBalance();
                    break;

                case 4:
                    status=false;
            }
        }
    }

}
