package Abstration;

public class AccountFactory {
    Account createAccount(int choice,double amt)
    {
        Account a1=null;
        if(choice==1)
        {
            a1=new savingAccount(amt);
        } else if (choice==2) {
            a1=new loanAccount(amt);

        }
        return a1;
    }
}
